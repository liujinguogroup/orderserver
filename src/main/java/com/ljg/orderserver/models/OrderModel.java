package com.ljg.orderserver.models;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
public class OrderModel implements Serializable {

    private Long id;
    private String userId;
    private String commodityCode;
    private Integer count;
    private Integer money;
}
