package com.ljg.orderserver.enums;


import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

@ApiModel(value = "OrderType",description = "订单类型")
@Getter
@AllArgsConstructor
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum OrderType {

    normal(0, "普通订单"),
    skill(1, "秒杀订单");

    @EnumValue
    @ApiModelProperty(value = "类型值", position = 0)
    private int value;

    @ApiModelProperty(value = "类型名称", position = 1)
    private String label;

}
