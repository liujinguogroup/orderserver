package com.ljg.orderserver.api;

import com.ljg.orderserver.entity.Order;
import com.ljg.orderserver.models.OrderModel;
import com.ljg.orderserver.service.OrderService;

import com.ljg.common.utils.exception.CustomException;
import com.ljg.common.utils.response.ResponseResult;
import com.ljg.esserver.entities.GoodsSearch;
import com.ljg.esserver.wrappers.GoodsDocWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "/order", tags = "订单接口")
@RestController
@RequestMapping("/order")
public class OrderController {

    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @Autowired
    private GoodsDocWrapper goodsDocWrapper;

    @ApiOperation("创建订单")
    @ApiParam(name = "model", type = "com.ljg.orderserver.models.OrderModel", value = "订单实体")
    @PostMapping("/create")
    public ResponseResult<Long> createOrder(@RequestBody OrderModel order){
        Long orderId = orderService.create(order);
        return ResponseResult.success(orderId);
    }

    @ApiOperation("创建订单")
    @ApiParam(name = "model", type = "com.example.ljg.entity.Order", value = "订单实体")
    @PostMapping("/select")
    public ResponseResult<Long> select(@RequestBody Order order) throws CustomException {
        goodsDocWrapper.search(GoodsSearch.builder()
                .id(order.getId()).build());
        return ResponseResult.success(order.getId());
    }
}
