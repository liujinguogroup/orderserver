package com.ljg.orderserver;

import com.ljg.common.annotation.EnableEnumMapper;
import com.ljg.common.annotation.EnableGlobalException;
import com.ljg.common.annotation.EnableSwaggerPlugin;
import com.ljg.common.mybatisplus.EnableCustomMyBatisPlusConfig;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@EnableGlobalException
@EnableCustomMyBatisPlusConfig
@EnableEnumMapper
@EnableSwaggerPlugin
//@EnabledCustomRibbonConfiguration
@EnableDiscoveryClient
@EnableFeignClients(basePackages = {"com.ljg"})
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@MapperScan("com.ljg.orderserver.mappers")
@ComponentScan(basePackages = {"com.ljg"})
public class OrderServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(OrderServerApplication.class, args);
    }


}
