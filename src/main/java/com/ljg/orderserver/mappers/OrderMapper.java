package com.ljg.orderserver.mappers;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ljg.orderserver.entity.Order;


public interface OrderMapper extends BaseMapper<Order> {
}
