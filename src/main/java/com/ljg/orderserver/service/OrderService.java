package com.ljg.orderserver.service;

import com.ljg.orderserver.entity.Order;
import com.ljg.orderserver.models.OrderModel;

public interface OrderService {

    /**
     * 创建订单
     */
    Long create(OrderModel order);
}