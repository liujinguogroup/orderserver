package com.ljg.orderserver.service.impl;

import com.ljg.orderserver.entity.Order;
import com.ljg.orderserver.mappers.OrderMapper;
import com.ljg.orderserver.models.OrderModel;
import com.ljg.orderserver.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
public class OrderServiceImpl implements OrderService {


    @Autowired
    private OrderMapper orderMapper;

    @Override
    @Transactional
    public Long create(OrderModel order) {
        log.info("创建订单");
        Order order1 = new Order();
        BeanUtils.copyProperties(order,order1);
        orderMapper.insert(order1);
        return order.getId();
    }

}
