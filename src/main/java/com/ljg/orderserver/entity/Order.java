package com.ljg.orderserver.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.Data;


@Data
@ApiModel(value = "Order",description = "")
@TableName(value = "order_tbl", autoResultMap = true)
public class Order {

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    @TableField(value = "user_id")
    private String userId;
    @TableField(value = "commodity_code")
    private String commodityCode;
    @TableField(value = "count")
    private Integer count;
    @TableField(value = "money")
    private Integer money;
//    @TableField(value = "count")
//
//    private OrderType orderType;
}
